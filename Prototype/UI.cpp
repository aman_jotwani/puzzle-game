#include "UI.hpp"
#include "Game.hpp"
#include "Tilemap.hpp"
#include <stdlib.h> // for atoi

#define local_persist static

NumTextSprite::NumTextSprite()
{
    cached_text[0] = '0';
    text_color = {0x00, 0xFF, 0x00, 0xFF};
}

void
NumTextSprite::Init(TTF_Font* numeric_font, SDL_Renderer* Renderer, SDL_Color* init_color)
{
    // so that i can give different colors to different numbers
    text_color = *init_color;
    LoadText(numeric_font, Renderer);
}

void
NumTextSprite::UpdateAndRender(int x, int y, int num, TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    int cached_num = atoi(&cached_text[0]);
    SDL_Rect temp = {x, y, width, height};

    // there will be many frames when the user is just standing on a tile
    // no need to run LoadText since num doesnt change.
    if(num != cached_num)
    {
        myitoa(num, cached_text, 10);
        LoadText(numeric_font, Renderer);
        temp.w = width;
        temp.h = height;
        SDL_RenderCopy(Renderer, tex, NULL, &temp);
    }
    else
        SDL_RenderCopy(Renderer, tex, NULL, &temp);
}

void
NumTextSprite::LoadText(TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    SDL_Surface* temp_surf = TTF_RenderText_Solid(numeric_font,
                                                  cached_text,
                                                  text_color);
    if(temp_surf == NULL)
    {
        Log("Text surface creation failed.\nSystem message - %s\n", GetError());
    }
    else
    {
        tex = SDL_CreateTextureFromSurface(Renderer, temp_surf);
        width = temp_surf->w;
        height = temp_surf->h;
        SDL_FreeSurface(temp_surf);
    }
}

void
Helper_Numbers::Init(TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    SDL_Color init_color = {0x00, 0xFF, 0x00, 0xFF};
    num_moves_text.Init(numeric_font, Renderer, &init_color);

    init_color = {0xFF, 0x00, 0x00, 0xFF};
    num_bombs_text.Init(numeric_font, Renderer, &init_color);
    max_moves_text.Init(numeric_font, Renderer, &init_color);
}

void
Helper_Numbers::UpdateAndRender(SDL_Renderer* Renderer, TTF_Font* numeric_font, int num_moves, int num_bombs, int max_moves)
{
    // i've heard that local_persist leads to bad bugs
    // lets see if i run into any of them.

    // the following code changes the color of num_moves as the player plays
    local_persist int change_point = max_moves/3;
    if(num_moves <= change_point)
    {
        num_moves_text.text_color = {0x00, 0xFF, 0x00, 0xFF};
    }
    else if(num_moves <= 2*change_point)
    {
        num_moves_text.text_color = {0xFF, 0xFF, 0x00, 0xFF};
    }
    else
    {
        num_moves_text.text_color = {0xFF, 0x00, 0x00, 0xFF};
    }
    
    num_moves_text.UpdateAndRender(8*TILE_WIDTH + 50,
                                   TILE_WIDTH,
                                   max_moves - num_moves,
                                   numeric_font, Renderer);

    local_persist SDL_Rect bomb_rect = {8*TILE_WIDTH + 100,
                                        2*TILE_WIDTH,
                                        32, 32};
    SDL_RenderCopy(Renderer, Game::bomb_sprite, NULL, &bomb_rect);

    num_bombs_text.UpdateAndRender(8*TILE_WIDTH + 50,
                                   2*TILE_WIDTH,
                                   num_bombs,
                                   numeric_font, Renderer);

    max_moves_text.UpdateAndRender(8*TILE_WIDTH + 50,
                                   4*TILE_WIDTH + 50,
                                   max_moves,
                                   numeric_font, Renderer);
}

void Path::Init(MatrixIndex* startpos)
{
    for(int i = 0; i < MAX_POINTS; i++)
    {
        path_points[i] = 0;
        path_buffer[i] = 0;
    }
    index = 0;
    visible = false;
    beginpos = *startpos;
}

void Path::Render(SDL_Renderer* Renderer)
{
    SDL_SetRenderDrawColor(Renderer, 0xFF, 0x00, 0x00, 0xFF);
    for(int i = 0; i <= MAP_WIDTH; i++)
    {
        SDL_RenderDrawLine(Renderer, i*TILE_WIDTH, 0, i*TILE_WIDTH, MAP_HEIGHT*TILE_WIDTH);
    }
    for(int i = 0; i < MAP_HEIGHT; i++)
    {
        SDL_RenderDrawLine(Renderer, 0, i*TILE_WIDTH, MAP_WIDTH*TILE_WIDTH, i*TILE_WIDTH);
    }
    
    SDL_Rect arrow_position = {0, 0, 32, 32};
    MatrixIndex pos = beginpos;
    for(int i = 0; i < 40; i++)
    {
        switch(path_points[i])
        {
        case Tilemap::LEFT:
        {
            arrow_position.x = TILE_WIDTH*(pos.col) + 16;
            arrow_position.y = TILE_WIDTH*(pos.row) + TILE_WIDTH/2 - 16;
            pos.col -= 1;
            SDL_RenderCopy(Renderer, Game::left_sprite, NULL, &arrow_position);
        }
        break;
        case Tilemap::RIGHT:
        {
            arrow_position.x = TILE_WIDTH*(pos.col + 1) - 48;
            arrow_position.y = TILE_WIDTH*pos.row + TILE_WIDTH/2 - 16;
            pos.col += 1;
            SDL_RenderCopy(Renderer, Game::right_sprite, NULL, &arrow_position);
        }
        break;
        case Tilemap::UP:
        {
            arrow_position.x = TILE_WIDTH*(pos.col) + TILE_WIDTH/2 - 16;
            arrow_position.y = TILE_WIDTH*pos.row + 16;
            pos.row -= 1;
            SDL_RenderCopy(Renderer, Game::up_sprite, NULL, &arrow_position);
        }
        break;
        case Tilemap::DOWN:
        {
            arrow_position.x = TILE_WIDTH*(pos.col) + TILE_WIDTH/2 - 16;
            arrow_position.y = TILE_WIDTH*(pos.row+1) - 48;
            pos.row += 1;
            SDL_RenderCopy(Renderer, Game::down_sprite, NULL, &arrow_position);
        }
        break;
        }
    }
}

void
Path::Flip()
{
    for(int i = 0; i < MAX_POINTS; i++)
    {
        path_points[i] = path_buffer[i];
        path_buffer[i] = 0;
    }
}

void
Path::Update(uint8 direction)
{
    switch(direction)
    {
    case Tilemap::LEFT:
    {
        path_buffer[index] = Tilemap::LEFT;
        index += 1;
    }
    break;
            
    case Tilemap::RIGHT:
    {
        path_buffer[index] = Tilemap::RIGHT;
        index += 1;
    }
    break;
            
    case Tilemap::UP:
    {
        path_buffer[index] = Tilemap::UP;
        index += 1;
    }
    break;
            
    case Tilemap::DOWN:
    {
        path_buffer[index] = Tilemap::DOWN;
        index += 1;
    }
    break;

    case 0:
    {
        index = 0;
        Flip();
    }
    break;
    }
}

void
LevelSelector::Init(TTF_Font* numeric_font, SDL_Renderer* Renderer)
{
    SDL_Color num_color = {0x00, 0x00, 0xFF, 0xFF};
    for(int i = 0; i < NUM_LEVELS; i++)
    {
        level_nums[i].Init(numeric_font, Renderer, &num_color);
    }
}

uint8
LevelSelector::HandleEvents(SDL_Event* e)
{
    if(e->type == SDL_MOUSEBUTTONDOWN && e->button.button == SDL_BUTTON_LEFT)
    {
        for(int i = 1; i <= NUM_LEVELS; i++)
        {
            if((e->button.x >= 350*i && e->button.x <= 350*i + TILE_WIDTH) &&
               (e->button.y >= 300 && e->button.y <= 300 + TILE_WIDTH))
            {
                return i;
            }
        }
        return 0;
    }
    return 0;
}

void
LevelSelector::Render(SDL_Renderer* Renderer, TTF_Font* numeric_font)
{
    SDL_SetRenderDrawColor(Renderer, 0x00, 0xFF, 0x00, 0xFF);
    SDL_Rect temp = {200, 300, TILE_WIDTH, TILE_WIDTH};

    for(int i = 0; i < NUM_LEVELS; i++)
    {
        temp.x += TILE_WIDTH;
        SDL_RenderDrawRect(Renderer, &temp);
        level_nums[i].UpdateAndRender(temp.x + 60, temp.y + 60, i+1, numeric_font, Renderer);
    }
    
}
    
