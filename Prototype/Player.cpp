#include "Player.hpp"
#include "Game.hpp"
#include <stdio.h>

Player::Player()
{
    startpos = {4, 0};
    Init(0);
}

Player::Player(Tilemap* tm)
{
    tmap = tm;
    startpos = {4, 0};
    Init(0);
}

void 
Player::SetTilemap(Tilemap* tm)
{
    tmap = tm;
}

void
Player::Init(uint8 max_moves)
{
    position = startpos;
    rect = {position.col*TILE_WIDTH + TILE_WIDTH/2 - 16,
            position.row*TILE_WIDTH + TILE_WIDTH/2 - 16,
            32, 32};

    if(curr_tile == NULL)
        curr_tile = (Tile*)malloc(sizeof(Tile));
    
    curr_tile = &(tmap->map[position.row][position.col]);
        

    right = false;
    left = false;
    up = false;
    down = false;
    
    num_bombs = 0;
    num_moves = 0;
    move_limit =  max_moves;
}

void
Player::HandleEvents(SDL_Event* e)
{
    if(e->type == SDL_KEYDOWN)
    {
        if(e->key.keysym.sym == SDLK_RIGHT)
        {
            right = true;
            left = false;
            down = false;
            up = false;
        }
        else if(e->key.keysym.sym == SDLK_LEFT)
        {
            left = true;
            right = false;
            down = false;
            up = false;
        }
        else if(e->key.keysym.sym == SDLK_UP)
        {
            up = true;
            right = false;
            down = false;
            left = false;
        }
        else if(e->key.keysym.sym == SDLK_DOWN)
        {
            down = true;
            up = false;
            left = false;
            right = false;
        }
        else if(e->key.keysym.sym == SDLK_SPACE)
        {
            ActionKeyPressed();
        }
        else if(e->key.keysym.sym == SDLK_g)
        {
            if(tmap->portal_pair->enabled)
                tmap->AddPortal(position.row, position.col);
        }
        else if(e->key.keysym.sym == SDLK_f)
        {
            if(tmap->portal_pair->enabled)
                tmap->DeletePortal(position.row, position.col);
        }
        else if(e->key.keysym.sym == SDLK_h)
        {
            if(tmap->portal_pair->enabled)
            {
                // reset on deleting both portals
                tmap->DeleteBothPortals();
                Reset();
            }
        }
        else if(e->key.keysym.sym == SDLK_r)
        {
            Reset();
        }
    }
}

void
Player::Reset()
{
    Init(move_limit);
    tmap->Reset();
    just_reset = true;
}

void
Player::SetStartPos(uint8 startx, uint8 starty)
{
    startpos = {startx, starty};
}

uint8
Player::UpdateAndRender(SDL_Renderer* Renderer)
{
    uint32 curr_bitfield = curr_tile->bitfield;
    uint8 rval = 9;// cant use 1, 2, 4, 8 and 0
    
    if(right)
    {
        if(IS_OFF(curr_bitfield, Tilemap::RIGHT_OBSTACLE) &&
           IS_ON(curr_bitfield, Tilemap::RIGHT))
        {
            // move right
            position.col += 1;
            num_moves += 1;
            rval = Tilemap::RIGHT;
        }
    }
    
    else if(left)
    {
        if(IS_OFF(curr_bitfield, Tilemap::LEFT_OBSTACLE) &&
           IS_ON(curr_bitfield, Tilemap::LEFT))
        {
            // move left
            position.col -= 1;
            num_moves += 1;
            rval = Tilemap::LEFT;
        }
    }
    
    else if(up)
    {
        if(IS_OFF(curr_bitfield, Tilemap::UP_OBSTACLE) &&
           IS_ON(curr_bitfield, Tilemap::UP))
        {
            // move up
            position.row -= 1;
            num_moves += 1;
            rval = Tilemap::UP;
        }
    }
    
    else if(down)
    {
        if(IS_OFF(curr_bitfield, Tilemap::DOWN_OBSTACLE) &&
           IS_ON(curr_bitfield, Tilemap::DOWN))
        {
            // move down
            position.row += 1;
            num_moves += 1;
            rval = Tilemap::DOWN;
        }
    }

    curr_tile = &(tmap->map[position.row][position.col]);
    
    if(IS_OFF(curr_tile->bitfield, Tilemap::VISIBLE))
    {
        curr_tile->bitfield ^= Tilemap::VISIBLE;
        tmap->map[position.row][position.col] = *curr_tile;
    }

    rect.x = position.col*TILE_WIDTH + TILE_WIDTH/2 - 16;
    rect.y = position.row*TILE_WIDTH + TILE_WIDTH/2 - 16;
    right = false;
    down = false;
    up = false;
    left = false;
    
    if(num_moves > move_limit)
    {
        // NOTE: Should this be moved to Game.cpp?
        // I think not, Should be factored out as a function in
        // Player itself since its used at multiple positions
        // and keeping it this way hides that fact.
        Reset();
    }

    if(just_reset)
    {
        rval = 0;
        just_reset = false;
    }
    
    //render
    SDL_SetRenderDrawColor(Renderer, 0xFF, 0x00, 0x00, 0xFF);
    SDL_RenderFillRect(Renderer, &rect);

    return rval;
}

/*
  NOTE:
  This function is based on the assumption
  that any given tile can have one of 
  obstacle, bomb, knob and any other mechanic I
  might add.
  This also means portals cant be on tiles with
  obstacles, bombs or knobs.
 */
void
Player::ActionKeyPressed()
{
    uint32 obstacle_mask = Tilemap::UP_OBSTACLE | Tilemap::DOWN_OBSTACLE | Tilemap::LEFT_OBSTACLE | Tilemap::RIGHT_OBSTACLE;
    uint32 bomb_mask = Tilemap::BOMB1;
    
    if(!IS_OFF(curr_tile->bitfield, obstacle_mask)) // if current tile has obstacles
    {
        if(num_bombs > 0)
        {
            int obstacle_blown = tmap->BlowObstacleAt(&position, num_bombs); 
            num_bombs -= obstacle_blown;
        }
    }
    else if(IS_ON(curr_tile->bitfield, bomb_mask)) // if current tile has bombs
    {
        int bomb_picked = tmap->PickBombAt(&position);
        num_bombs += bomb_picked;
    }
    else if(IS_ON(curr_tile->bitfield, Tilemap::PORTAL))
    {
        tmap->GetPortDest(&position);
    }
    // assuming its a knob
    else
    {
        tmap->TurnKnob(&position);
    }
}
