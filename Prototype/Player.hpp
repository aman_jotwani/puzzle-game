#pragma once
#include "Prototype.hpp"
#include "Tilemap.hpp"
#include "Utils.hpp"

class Player
{
public:
    Player();
    Player(Tilemap* tm);
    
    SDL_Rect rect;
    Tile* curr_tile;
    Tilemap* tmap;
    int num_bombs;
    bool right;
    bool left;
    bool up;
    bool down;            
    uint8 num_moves;
    uint8 move_limit;
    MatrixIndex position;
    MatrixIndex startpos;
    bool just_reset;
    
    void Init(uint8 max_moves);
    void SetStartPos(uint8 startx, uint8 starty);
    
    void ActionKeyPressed();
    
    void SetTilemap(Tilemap* tm);
    void HandleEvents(SDL_Event* e);
    uint8 UpdateAndRender(SDL_Renderer* Renderer);
    void Reset();
}; // size = 2+4+4+4+4+4+2 = 24 bytes
