#include "GameObjects.hpp"
#include "Game.hpp"

PortalPair::PortalPair()
{
    portal_1 = {255, 255};
    portal_2 = {255, 255};
    enabled = false;
}

/* 
NOTE:
Checks if portals were placed on tiles with obstacles or bombs or knobs/switches.
Static portals dont need to call this if created with care in the
editor.
*/
void
PortalPair::Setup(Tilemap* tilemap)
{
    uint32 portal_mask = Tilemap::UP_OBSTACLE | Tilemap::DOWN_OBSTACLE |
        Tilemap::LEFT_OBSTACLE | Tilemap::RIGHT_OBSTACLE | Tilemap::BOMB1;

    if(VALID_INDEX(&portal_1))
    {
        bool portal_on_knob = false;
        if(tilemap->coupled_switch->enabled)
        {
            portal_on_knob = (EQUAL_INDEX(&portal_1, &tilemap->coupled_switch->knob1_indices) ||
                              EQUAL_INDEX(&portal_1, &tilemap->coupled_switch->knob2_indices) ||
                              EQUAL_INDEX(&portal_1, &tilemap->coupled_switch->switch_indices)) ;
        }
        
        if(IS_OFF(tilemap->map[portal_1.row][portal_1.col].bitfield, portal_mask) && !portal_on_knob)
        {
            tilemap->map[portal_1.row][portal_1.col].bitfield ^= Tilemap::PORTAL;
        }
        else
            portal_1 = {255, 255};
    }
    if(VALID_INDEX(&portal_2))
    {
        bool portal_on_knob = false;
        if(tilemap->coupled_switch->enabled)
        {
            portal_on_knob = (EQUAL_INDEX(&portal_2, &tilemap->coupled_switch->knob1_indices) ||
                              EQUAL_INDEX(&portal_2, &tilemap->coupled_switch->knob2_indices) ||
                              EQUAL_INDEX(&portal_2, &tilemap->coupled_switch->switch_indices)) ;
        }
        
        if(IS_OFF(tilemap->map[portal_2.row][portal_2.col].bitfield, portal_mask) && !portal_on_knob)
        {
            tilemap->map[portal_2.row][portal_2.col].bitfield ^= Tilemap::PORTAL;
        }
        else
            portal_2 = {255, 255};
    }
}

void
PortalPair::UpdateAndRender(SDL_Renderer* Renderer, bool p1_visible, bool p2_visible)
{
    
    SDL_Rect temp = {0, 0, TILE_WIDTH, TILE_WIDTH};
    if(VALID_INDEX(&portal_1) && p1_visible)
    {
        temp.x = portal_1.col*TILE_WIDTH;
        temp.y = portal_1.row*TILE_WIDTH;
        SDL_SetRenderDrawColor(Renderer, 0x00, 0x00, 0xFF, 0xFF);
        SDL_RenderFillRect(Renderer, &temp);
    }
    if(VALID_INDEX(&portal_2) && p2_visible)
    {
        temp.x = portal_2.col*TILE_WIDTH;
        temp.y = portal_2.row*TILE_WIDTH;
        SDL_SetRenderDrawColor(Renderer, 0xFF, 0x66, 0x00, 0xFF);
        SDL_RenderFillRect(Renderer, &temp);
    }
}

CoupledSwitch::CoupledSwitch()
{
    knob1_indices = {255, 255};
    knob2_indices = {255, 255};
    switch_indices = {255, 255};
    affected_indices = {255, 255};
    knob1_on = false;
    knob2_on = false;
    enabled = false;
    obstacle_affected = 0;
}

void
CoupledSwitch::Setup()
{
    knob1_on = false;
    knob2_on = false;
}

void
CoupledSwitch::UpdateAndRender(SDL_Renderer* Renderer, bool knob1_visible, bool knob2_visible, bool switch_visible)
{
    SDL_Rect knob_rect = {0, 0, 70, 70};
    SDL_Rect switch_rect = {0, 0, 70, 70};

    if(knob1_visible)
    {
        knob_rect.x = knob1_indices.col*TILE_WIDTH;
        knob_rect.y = knob1_indices.row*TILE_WIDTH;

        if(knob1_on)
        {
            SDL_RenderCopy(Renderer, Game::knob_on_sprite, NULL, &knob_rect);
        }
        else
        {
            SDL_RenderCopy(Renderer, Game::knob_off_sprite, NULL, &knob_rect);
        }
    }
    
    if(knob2_visible)
    {
        knob_rect.x = knob2_indices.col*TILE_WIDTH;
        knob_rect.y = knob2_indices.row*TILE_WIDTH;

        if(knob2_on)
        {
            SDL_RenderCopy(Renderer, Game::knob_on_sprite, NULL, &knob_rect);
        }
        else
        {
            SDL_RenderCopy(Renderer, Game::knob_off_sprite, NULL, &knob_rect);
        }
    }

    if(switch_visible)
    {
        switch_rect.x = switch_indices.col*TILE_WIDTH;
        switch_rect.y = switch_indices.row*TILE_WIDTH;

        if(knob1_on)
        {
            SDL_RenderCopy(Renderer, Game::switch_one_on, NULL, &switch_rect);
        }
        else
        {
            SDL_RenderCopy(Renderer, Game::switch_one_off, NULL, &switch_rect);
        }

        switch_rect.x = switch_indices.col*TILE_WIDTH;
        switch_rect.y = switch_indices.row*TILE_WIDTH + TILE_WIDTH/2 - 16;

        if(knob2_on)
        {
            SDL_RenderCopy(Renderer, Game::switch_two_on, NULL, &switch_rect);
        }
        else
        {
            SDL_RenderCopy(Renderer, Game::switch_two_off, NULL, &switch_rect);
        }
    }
}
