#include <stdio.h>
#include <stdlib.h>
#include "Game.hpp"

SDL_Texture* Game::rock_sprite = NULL;
SDL_Texture* Game::bomb_sprite = NULL;
SDL_Texture* Game::up_sprite = NULL;
SDL_Texture* Game::down_sprite = NULL;
SDL_Texture* Game::right_sprite = NULL;
SDL_Texture* Game::left_sprite = NULL;
SDL_Texture* Game::knob_on_sprite = NULL;
SDL_Texture* Game::knob_off_sprite = NULL;
SDL_Texture* Game::switch_one_off = NULL;
SDL_Texture* Game::switch_one_on = NULL;
SDL_Texture* Game::switch_two_on = NULL;
SDL_Texture* Game::switch_two_off = NULL;
SDL_Texture* Game::end_sprite = NULL;
SDL_Texture* Game::ui_one = NULL;
SDL_Texture* Game::ui_two = NULL;
SDL_Texture* Game::dynamite_sprite = NULL;
TTF_Font*    Game::numeric_font = NULL;
char         Game::config_fname[11] = "config.txt";

Game::Game(SDL_Renderer* Renderer)
{
    // TODO: ReadConfigFile(); only to load where you left off.
    curr_level_num = 1;
    game_state = IN_GAME;
    PreloadAssets(Renderer);
    player.SetTilemap(&tilemap);
    nums_text.Init(numeric_font, Renderer);
    last_path.Init(&player.startpos);
    LoadLevel(curr_level_num);
}

void
Game::HandleEvents(SDL_Event* e)
{
    if(e->type == SDL_KEYDOWN)
    {
        if(e->key.keysym.sym == SDLK_t)
        {
            editor.Toggle(&tilemap, &player);
            if(editor.enabled)
                game_state = EDITOR_MODE;
            else
            {
                // NOTE: this is a hack. for now.
                last_path.Init(&player.startpos);
                game_state = IN_GAME;
            }
        }
        else if(editor.enabled)
        {
            editor.HandleEvents(e);
        }
        else
        {
            if(e->key.keysym.sym == SDLK_TAB)
            {
                if(!last_path.visible)
                {
                    last_path.visible = true;
                    game_state = LAST_PATH_SCREEN;
                }
                else
                {
                    last_path.visible = false;
                    game_state = IN_GAME; 
                }
            }
            else if(e->key.keysym.sym == SDLK_c)
                game_state = game_state == CONTROLS_SCREEN ? IN_GAME : CONTROLS_SCREEN; 
            else
                player.HandleEvents(e);
        }
    }
}

void
Game::GameOverScreen()
{
    
}

void
Game::ReadConfigFile()
{
    
}

void
Game::LoadLevel(uint32 level_num)
{
    char fname[20];
    FILE* fptr;
    sprintf(fname, "Levels/Level%d.txt", level_num);
    fptr = fopen(fname, "r");

    uint8 row = 0;
    uint8 col = 0;
    int bitfield = 0;
    char map_data[100];
    int num_elems = MAP_WIDTH*MAP_HEIGHT;

    // reading max moves
    fscanf(fptr, "%s", map_data);
    uint8 move_limit = atoi(&map_data[10]);

    // reading player startpos
    fscanf(fptr, "%s", map_data);
    row = atoi(&map_data[9]);
    col = atoi(&map_data[11]);
    MatrixIndex startpos = {row, col};
    
    // reading map data
    while(num_elems != 0)
    {
        /*
          map[%d][%d].bitfield=%d
              4   7            19
         Same logic for switches.
         */
        
        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[4]);
        col = atoi(&map_data[7]);
        bitfield = atoi(&map_data[19]);
        tilemap.init_data[row][col].bitfield = bitfield;
        tilemap.map[row][col].bitfield = bitfield;
        if(IS_ON(bitfield, Tilemap::END))
            tilemap.end_pos = {row, col};
        num_elems -= 1;
    }
    
    // read switch data
    // COUPLED_SWITCH_ENABLED=%d
    //                        23
    int switch_enabled = 0;
    fscanf(fptr, "%s", map_data);
    switch_enabled = atoi(&map_data[23]);
    if(switch_enabled)
    {
        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[15]);
        col = atoi(&map_data[17]);
        tilemap.coupled_switch->knob1_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[15]);
        col = atoi(&map_data[17]);
        tilemap.coupled_switch->knob2_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[16]);
        col = atoi(&map_data[18]);
        tilemap.coupled_switch->switch_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[18]);
        col = atoi(&map_data[20]);
        tilemap.coupled_switch->affected_indices = {row, col};

        fscanf(fptr, "%s", map_data);
        bitfield = atoi(&map_data[18]);
        tilemap.coupled_switch->obstacle_affected = bitfield;

        tilemap.coupled_switch->knob1_on = false;
        tilemap.coupled_switch->knob2_on = false;
        tilemap.coupled_switch->enabled = true;
    }
    else
        tilemap.coupled_switch->enabled = false;

    // read static portals if present
    // STATIC_PORTALS_ENABLED=%d
    //                        23
    int sp_enabled = 0;
    fscanf(fptr, "%s", map_data);
    sp_enabled = atoi(&map_data[23]);
    if(sp_enabled)
    {
        // sp1_position=%d,%d
        //              13, 15 
        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[13]);
        col = atoi(&map_data[15]);
        tilemap.static_portal_pair->portal_1 = {row, col};

        fscanf(fptr, "%s", map_data);
        row = atoi(&map_data[13]);
        col = atoi(&map_data[15]);
        tilemap.static_portal_pair->portal_2 = {row, col};

        tilemap.static_portal_pair->enabled = true;
    }
    else
        tilemap.static_portal_pair->enabled = false;

    // DYNAMIC_PORTALS_ENABLED=%d
    //                         24
    int dp_enabled = 0;
    fscanf(fptr, "%s", map_data);
    dp_enabled = atoi(&map_data[24]);
    if(dp_enabled)
        tilemap.portal_pair->enabled = true;
    else
        tilemap.portal_pair->enabled = false;

    player.SetStartPos(startpos.row, startpos.col);
    player.Init(move_limit);
    last_path.Init(&player.startpos);
    game_state = IN_GAME;
}

void
Game::UpdateAndRender(SDL_Renderer* Renderer)
{
    if(game_state == EDITOR_MODE)
    {
        editor.UpdateAndRender(Renderer);
    }
    else if(game_state == LAST_PATH_SCREEN)
    {
        last_path.Render(Renderer);
    }
    else if(game_state == IN_GAME)
    {
        tilemap.UpdateAndRender(Renderer);
        uint8 direction = player.UpdateAndRender(Renderer);
        last_path.Update(direction);

        // if level cleared.
        if(EQUAL_INDEX(&player.position, &tilemap.end_pos))
        {
            if(curr_level_num == MAX_LEVELS)
            {
                game_state = GAME_OVER_SCREEN;
                // GameOverScreen();
            }
            else
            {
                curr_level_num += 1;
                last_path.Init(&player.startpos);
                LoadLevel(curr_level_num);
            }
        }
        
        nums_text.UpdateAndRender(Renderer,
                                  numeric_font,
                                  player.num_moves,
                                  player.num_bombs,
                                  player.move_limit);
    }
    else if(game_state == GAME_OVER_SCREEN)
    {
        // TODO: Render GAMEOVER.
    }
    else if(game_state == CONTROLS_SCREEN)
    {
        // TODO: Render Controls
    }
    
    
}

void
Game::PreloadAssets(SDL_Renderer* Renderer)
{
    rock_sprite = LoadImage("Rock.png", Renderer);
    bomb_sprite = LoadImage("bomb.png", Renderer);
    up_sprite = LoadImage("assets/up.png", Renderer);
    down_sprite = LoadImage("assets/down.png", Renderer);
    right_sprite = LoadImage("assets/right.png", Renderer);
    left_sprite = LoadImage("assets/left.png", Renderer);
    knob_off_sprite = LoadImage("assets/switchLeft.png", Renderer);
    knob_on_sprite = LoadImage("assets/switchRight.png", Renderer);
    switch_one_off = LoadImage("assets/buttonBlue.png", Renderer);
    switch_one_on = LoadImage("assets/buttonBlue_pressed.png", Renderer);
    switch_two_off = LoadImage("assets/buttonRed.png", Renderer);
    switch_two_on = LoadImage("assets/buttonRed_pressed.png", Renderer);
    end_sprite = LoadImage("assets/hud_heartFull.png", Renderer);
    ui_one = LoadImage("assets/hud_1.png", Renderer);
    ui_two = LoadImage("assets/hud_2.png", Renderer);
    dynamite_sprite = LoadImageWithColorKey("Dynamite.png", Renderer, 0xFF, 0xFF, 0xFF);
    numeric_font = TTF_OpenFont("Shoes_center.ttf", 50);
}
