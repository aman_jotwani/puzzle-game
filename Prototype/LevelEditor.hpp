#pragma once
#include "Prototype.hpp"
#include "Utils.hpp"
#include "Tilemap.hpp"
#include "Player.hpp"
#include "GameObjects.hpp"

class Editor
{
public:
    static const uint32 ROWS = 5; 
    static const uint32 COLS = 8;
    bool dynamic_portals_enabled;
    bool enabled;
    uint8 move_limit;
    Tile map[ROWS][COLS];
    CoupledSwitch coupled_switch;
    PortalPair static_portal_pair;
    Tile* curr_tile;
    MatrixIndex curr_indices;
    MatrixIndex player_startpos;

    Editor();
    void Toggle(Tilemap* real_map, Player* player);
    // currently Tilemap and Player sent HandleEvents
    // to just call toggle immediately after load.
    // better ways to do the same? Any reason why this is a Bad Thing?
    void HandleEvents(SDL_Event* e); 
    void Load();
    void Save();
    void UpdateAndRender(SDL_Renderer* Renderer);
    MatrixIndex GetIndices(int screen_x, int screen_y);
}; // size =  8+160+2+4+11+5+3 = 173
