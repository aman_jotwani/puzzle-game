#pragma once
#include "Prototype.hpp"
#include "Tilemap.hpp"
#include "Player.hpp"
#include "LevelEditor.hpp"
#include "UI.hpp"
    
class Game
{
public:
    static const int MAX_LEVELS = 6;
    static SDL_Texture* rock_sprite;
    static SDL_Texture* bomb_sprite;
    static SDL_Texture* up_sprite;
    static SDL_Texture* down_sprite;
    static SDL_Texture* right_sprite;
    static SDL_Texture* left_sprite;
    static SDL_Texture* knob_on_sprite;
    static SDL_Texture* knob_off_sprite;
    static SDL_Texture* switch_one_off;
    static SDL_Texture* switch_one_on;
    static SDL_Texture* switch_two_on;
    static SDL_Texture* switch_two_off;
    static SDL_Texture* end_sprite;
    static SDL_Texture* ui_one;
    static SDL_Texture* ui_two;
    static SDL_Texture* dynamite_sprite;
    static TTF_Font* numeric_font;
    static char config_fname[11];

    static const int EDITOR_MODE      = 0;
    static const int IN_GAME          = 1;
    static const int CONTROLS_SCREEN  = 2;
    static const int LAST_PATH_SCREEN = 3;
    static const int GAME_OVER_SCREEN = 4;
    
    Tilemap tilemap;
    Editor editor;
    Player player;
    Helper_Numbers nums_text;
    Path last_path;
    int curr_level_num;
    int game_state;
    
    Game(SDL_Renderer* Renderer);
    void PreloadAssets(SDL_Renderer* Renderer);
    void HandleEvents(SDL_Event* e);
    void LoadLevel(uint32 level_num);
    void UpdateAndRender(SDL_Renderer* Renderer);
    void ReadConfigFile();
    void GameOverScreen();
};
