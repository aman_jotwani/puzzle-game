#include "Utils.hpp"

void strreverse(char* begin, char* end) {
	
	char aux;
	
	while(end>begin)
	
		aux=*end, *end--=*begin, *begin++=aux;
	
}

// NOTE: Copy pasted code. Use your own or use this till it breaks.
// sadly, itoa is not part of the standard and not cross platform.
void myitoa(int value, char* str, int base) {
	
	static char num[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	
	char* wstr=str;
	
	int sign;
	

	
	// Validate base
	
	if (base<2 || base>35){ *wstr='\0'; return; }
	

	
	// Take care of sign
	
	if ((sign=value) < 0) value = -value;
	

	
	// Conversion. Number is reversed.
	
	do *wstr++ = num[value%base]; while(value/=base);
	
	if(sign<0) *wstr++='-';
	
	*wstr='\0';
	

	
	// Reverse string
	
	strreverse(str,wstr-1);
	
}

bool IS_ON(uint32 bitfield, uint32 mask)
{
    if((bitfield & mask) == mask)
        return true;
    else
        return false;
}

bool IS_OFF(uint32 bitfield, uint32 mask)
{
    if((bitfield & mask) == 0)
        return true;
    else
        return false;
}

bool VALID_INDEX(MatrixIndex* indices)
{
    if(indices->row == 255 && indices->col == 255)
    {
        return false;
    }
    else if(indices->row < MAP_HEIGHT && indices->col < MAP_WIDTH)
    {
        return true;
    }
    else
        return false;
}

bool EQUAL_INDEX(MatrixIndex* first, MatrixIndex* second)
{
    if(first->row == second->row && first->col == second->col)
        return true;
    else
        return false;
}

/*
Initialize all SDL systems.
Preps the window and renderer.
Returns 1 on success, 0 on failure.
 */
int
InitSDL(SDL_Window** Window, SDL_Renderer** Renderer, char* title, int width, int height)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0){
        Log("SDL didnt initialize. System message -- %s\n", GetError());
        return 0;
    }
    else{
        *Window = SDL_CreateWindow(title,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   width, height,
                                   SDL_WINDOW_SHOWN);
        if (*Window == NULL){
            Log("Window creation error.\nSystem message -- %s\n", GetError());
            return 0;
        }
        else{
            *Renderer = SDL_CreateRenderer(*Window, -1, SDL_RENDERER_ACCELERATED);
            if (*Renderer == NULL)
            {
                Log("Renderer initialization failed.\nSystem message - %s\n", GetError());
                return 0;
            }
            else
            {
                if(IMG_Init(IMG_INIT_PNG) == IMG_INIT_PNG)
                {
                    if(TTF_Init() == -1)
                    {
                        Log("SDL_ttf could not be initialized!\nSystem message - %s\n", GetError());
                        return 0;
                    }
                    else
                    {
                        Log("All Systems Go.");
                        return 1;
                    }
                    
                    // if(IS_ON(Mix_Init(MIX_INIT_FLAC), MIX_INIT_FLAC))
                    // {
                    //     if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) == -1)
                    //     {
                    //         Log("Audio opening failed.\nSystem message - %s\n", GetError());
                    //         return 0;
                    //     }
                    //     Log("All Systems Go.");
                    //     return 1;
                    // }
                    // else
                    // {
                    //     Log("Audio module initialization failed.\nSystem message - %s\n", GetError());
                    //     return 0;
                    // }
                }
                else
                {
                    Log("Image module initialization failed.\nSystem message - %s\n", GetError());
                    return 0;
                }
            }
        }
    }
}

SDL_Texture* LoadImage(char* filename, SDL_Renderer* Renderer)
{
    SDL_Texture* temp = NULL;
    
    SDL_Surface* surf = IMG_Load(filename);
    if(surf == NULL)
    {
        Log("Image loader failed for %s.\n", filename);
    }
    else
    {
        temp = SDL_CreateTextureFromSurface(Renderer, surf);
        if(temp == NULL)
        {
            Log("Texture creation from surface failed.\n");
        }
        else
        {
            SDL_FreeSurface(surf);
        }
    }
    return temp;
}

SDL_Texture* LoadImageWithColorKey(char* filename, SDL_Renderer* Renderer, uint8 r, uint8 g, uint8 b)
{
    SDL_Texture* temp = NULL;
    
    SDL_Surface* surf = IMG_Load(filename);
    if(surf == NULL)
    {
        Log("Image loader failed.\n");
    }
    else
    {
        SDL_SetColorKey(surf, SDL_TRUE, SDL_MapRGB(surf->format, r, g, b));
        temp = SDL_CreateTextureFromSurface(Renderer, surf);
        if(temp == NULL)
        {
            Log("Texture creation from surface failed.\n");
        }
        else
        {
            SDL_FreeSurface(surf);
        }
    }
    return temp;
}
