coupled_switch = 2, 2
knob1 = 0, 4
knob2 = 4, 4
obstacle connected to coupled_switch = 2, 3

map[0][0].bitfield = (RIGHT | DOWN);
map[0][1].bitfield = (RIGHT | LEFT | DOWN);
map[0][2].bitfield = (RIGHT | LEFT | DOWN);
map[0][3].bitfield = (RIGHT | LEFT | DOWN);
map[0][4].bitfield = (LEFT);
map[0][5].bitfield = (RIGHT | DOWN);
map[0][6].bitfield = (LEFT | DOWN | DOWN_OBSTACLE);

map[1][0].bitfield = (UP | DOWN);
map[1][1].bitfield = (UP | DOWN);
map[1][2].bitfield = (UP | DOWN);
map[1][3].bitfield = (UP | DOWN);
map[1][4].bitfield = (RIGHT | DOWN | BOMB1);
map[1][5].bitfield = (UP | DOWN | LEFT);
map[1][6].bitfield = (UP | DOWN | DOWN_OBSTACLE);

map[2][0].bitfield = (UP | DOWN | RIGHT);
map[2][1].bitfield = (UP | DOWN | RIGHT | LEFT);
map[2][2].bitfield = (UP | RIGHT | LEFT);
map[2][3].bitfield = (UP | RIGHT | LEFT | DOWN | RIGHT_OBSTACLE);
map[2][4].bitfield = (UP | DOWN | LEFT);
map[2][5].bitfield = (UP | DOWN);
map[2][6].bitfield = (END);

map[3][0].bitfield = (UP | DOWN | RIGHT);
map[3][1].bitfield = (UP | DOWN | LEFT | RIGHT);
map[3][2].bitfield = (RIGHT | LEFT | DOWN);
map[3][3].bitfield = (UP | DOWN | LEFT);
map[3][4].bitfield = (UP | BOMB1);
map[3][5].bitfield = (UP | DOWN | LEFT);
map[3][6].bitfield = (UP | DOWN | UP_OBSTACLE);

map[4][0].bitfield = (VISIBLE | UP | RIGHT);
map[4][1].bitfield = (UP | RIGHT | LEFT);
map[4][2].bitfield = (UP | RIGHT | LEFT);
map[4][3].bitfield = (UP | LEFT | RIGHT);
map[4][4].bitfield = (LEFT);
map[4][5].bitfield = (UP | RIGHT | RIGHT_OBSTACLE);
map[4][6].bitfield = (UP | LEFT | UP_OBSTACLE);
