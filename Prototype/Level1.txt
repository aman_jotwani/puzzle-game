map[0][0].bitfield = (RIGHT | DOWN);

map[0][1].bitfield = (LEFT | DOWN);

map[0][2].bitfield = (BOMB1 | BOMB2 | DOWN);

map[0][3].bitfield = END; 

map[1][0].bitfield = (RIGHT | UP | DOWN);

map[1][1].bitfield = (UP | RIGHT | DOWN | LEFT);

map[1][2].bitfield = (LEFT | UP | UP_OBSTACLE);

map[1][3].bitfield = (UP | DOWN | UP_OBSTACLE | TWO_OBSTACLES);

map[2][0].bitfield = (UP | DOWN);

map[2][1].bitfield = (UP | DOWN | RIGHT);

map[2][2].bitfield = (RIGHT | LEFT);

map[2][3].bitfield = (LEFT | UP | DOWN);

map[3][0].bitfield = (UP | RIGHT | VISIBLE);

map[3][1].bitfield = (UP | LEFT);

map[3][2].bitfield = (BOMB1 | RIGHT);

map[3][3].bitfield = (UP | LEFT);
