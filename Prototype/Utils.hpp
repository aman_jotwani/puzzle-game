#pragma once
#include "Prototype.hpp"
#include <SDL_image.h>
#include <SDL_ttf.h>

struct Vec2
{
    float x;
    float y;
}; // 8 bytes

struct Vec2Int
{
    int x;
    int y;
}; // 8 bytes
       
struct MatrixIndex
{
    uint8 row;
    uint8 col;
}; // 2 bytes

void myitoa(int value, char* str, int base);
    
bool IS_ON(uint32 bitfield, uint32 mask);

bool IS_OFF(uint32 bitfield, uint32 mask);

int InitSDL(SDL_Window** Window, SDL_Renderer** Renderer, char* title, int width, int height);

SDL_Texture* LoadImage(char* filename, SDL_Renderer* Renderer);
SDL_Texture* LoadImageWithColorKey(char* filename, SDL_Renderer* Renderer, uint8 r, uint8 g, uint8 b);

bool VALID_INDEX(MatrixIndex* indices);

bool EQUAL_INDEX(MatrixIndex* first, MatrixIndex* second);
