#pragma once
#include "Prototype.hpp"
#include "Utils.hpp"
#include <SDL_ttf.h>

class NumTextSprite
{
public:
    SDL_Texture* tex;
    SDL_Color text_color;
    int width;
    int height;
    char cached_text[3];
    
    NumTextSprite();
    void Init(TTF_Font* numeric_font, SDL_Renderer* Renderer, SDL_Color* init_color);
    void LoadText(TTF_Font* numeric_font, SDL_Renderer* Renderer);
    void UpdateAndRender(int x, int y, int num, TTF_Font* numeric_font, SDL_Renderer* Renderer);
};

struct Helper_Numbers
{
    NumTextSprite num_moves_text;
    NumTextSprite num_bombs_text;
    NumTextSprite max_moves_text;

    void Init(TTF_Font* numeric_font, SDL_Renderer* Renderer);
    void UpdateAndRender(SDL_Renderer* Renderer, TTF_Font* numeric_font, int num_moves, int num_bombs, int max_moves);
};

struct Path
{
    static const int MAX_POINTS = 40;
    uint8 path_points[MAX_POINTS];
    uint8 path_buffer[MAX_POINTS];
    int index;
    MatrixIndex beginpos;
    bool visible;

    void Init(MatrixIndex* player_startpos);
    void Update(uint8 player_direction);
    void Render(SDL_Renderer* Renderer);
    void Flip();
};

struct LevelSelector
{
    static const int NUM_LEVELS = 3;
    NumTextSprite level_nums[NUM_LEVELS];
    
    void Init(TTF_Font* numeric_font, SDL_Renderer* Renderer);
    uint8 HandleEvents(SDL_Event* e);
    void Render(SDL_Renderer* Renderer, TTF_Font* numeric_font);
};
